### Service implementaion
```php
/**
 * Service implementaiton
 */
$provider = \Drupal::service('paypal_plus');
$payment_options = array(
  'amount' => '10',
  'currency_code' => 'USD',
);
$provider->paypalPay($payment_options);
```

### Available hooks on this module
```php
/**
 * Implements hook_paypal_plus_currency_code().
 */
function paypal_plus_paypal_plus_currency_code(&$currency_code)
{
    // Modify the currency value.
    // $currency_code = 'USD';
}
/**
 * Implements hook_paypal_plus_sandbox_mode().
 */
function paypal_plus_paypal_plus_sandbox_mode(&$sandbox)
{
    // Modify the sandbox mode.
    // $sandbox = true;
}
/**
 * Implements hook_paypal_plus_failure_message().
 */
function paypal_plus_paypal_plus_failure_message(&$message)
{
    // Modify the failure message.
    // $message = 'Failed';
}
/**
 * Implements hook_paypal_plus_success_message().
 */
function paypal_plus_paypal_plus_success_message(&$message)
{
    // Modify the success message.
    // $message = 'Success';
}
/**
 * Implements hook_paypal_plus_cancel_message().
 */
function paypal_plus_paypal_plus_cancel_message(&$message)
{
    // Modify the cancel message.
    // $message = 'canceled';
}
/**
 * Implements hook_paypal_plus_success_redirect().
 */
function paypal_plus_paypal_plus_success_redirect(&$redirect_url)
{
    // Modify the success redirect.
    // $redirect_url = ''; //full url
}
/**
 * Implements hook_paypal_plus_failure_redirect().
 */
function paypal_plus_paypal_plus_failure_redirect(&$redirect_url)
{
    // Modify the failure redirect.
    // $redirect_url = ''; //full url
}
/**
 * Implements hook_paypal_plus_set_item_ids().
 */
function paypal_plus_paypal_plus_set_item_ids(&$item_ids)
{
    // Set item ids for future processing
    // $item_ids = [];
}
```
