<?php

namespace Drupal\paypal_plus\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PaypalPayForm extends FormBase
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'newsletter_form';
    }

    /**
     * @param  array              $form
     * @param  FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['amount'] = array(
          '#type' => 'number',
          '#min' => '1',
          '#title' => $this->t('Amount'),
          '#required' => true,
      );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
          '#button_type' => 'primary',
      );
        return $form;
    }

    /**
     * Form submission handler.
     *
     * @param array                                $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $amount = $form_state->getValue('amount');
        $provider = \Drupal::service('paypal_plus');
        $payment_options = array(
          'amount' => $amount,
      );
        $provider->paypalPay($payment_options);
    }
}
