<?php

namespace Drupal\paypal_plus\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure Social sharing settings.
 */
class PaypalConfig extends ConfigFormBase
{
    /**
     * Drupal\Core\Extension\ModuleHandler definition.
     *
     * @var \Drupal\Core\Extension\ModuleHandler
     */
    protected $moduleHandler;

    /**
     * The entity type bundle info.
     *
     * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
     */
    protected $entityTypeBundleInfo;

    /**
     * Constructs a SocialSharingSettingsForm object.
     *
     * @param \Drupal\Core\Extension\ModuleHandler              $module_handler
     *   The factory for configuration objects.
     * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
     *   The entity type bundle info.
     */
    public function __construct(ModuleHandler $module_handler, EntityTypeBundleInfoInterface $entity_type_bundle_info)
    {
        $this->moduleHandler = $module_handler;
        $this->entityTypeBundleInfo = $entity_type_bundle_info;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('module_handler'),
            $container->get('entity_type.bundle.info')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'paypal_plus_config';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
          'paypal_plus.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $paypal_plus_settings = $this->config('paypal_plus.settings');

        $form['paypal_configuration'] = [
          '#type' => 'details',
          '#title' => $this->t('Paypal Plus Configuration'),
          '#open' => true,
        ];

        $form['paypal_configuration']['sandbox_mode'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Sandbox Mode'),
          '#default_value' => $paypal_plus_settings->get("sandbox_mode"),
        ];

        $form['paypal_configuration']['currency_code'] = [
          '#type' => 'select',
          '#title' => $this->t('Currency code'),
          '#required' => true,
          '#options' => get_currency_codes(),
          '#default_value' => $paypal_plus_settings->get("currency_code")??'USD',
          '#description' => $this->t('Paypal currency code.'),
        ];

        $form['paypal_configuration']['sandbox'] = [
          '#type' => 'details',
          '#title' => $this->t('Sandbox Configuration'),
          '#open' => $paypal_plus_settings->get("sandbox_mode"),
        ];

        $form['paypal_configuration']['sandbox']['sclient_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Id'),
          '#default_value' => $paypal_plus_settings->get("sclient_id"),
          '#description' => $this->t('Sandbox client id'),
        ];

        $form['paypal_configuration']['sandbox']['sclient_secret'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Secret'),
          '#default_value' => $paypal_plus_settings->get("sclient_secret"),
          '#description' => $this->t('Sandbox client secret'),
        ];

        $form['paypal_configuration']['live'] = [
          '#type' => 'details',
          '#title' => $this->t('Live Configuration'),
          '#open' => !$paypal_plus_settings->get("sandbox_mode"),
        ];

        $form['paypal_configuration']['live']['lclient_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Id'),
          '#default_value' => $paypal_plus_settings->get("lclient_id"),
          '#description' => $this->t('Live client id'),
        ];

        $form['paypal_configuration']['live']['lclient_secret'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Secret'),
          '#default_value' => $paypal_plus_settings->get("lclient_secret"),
          '#description' => $this->t('Live client secret'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        \Drupal::messenger()->addMessage($this->t('Your configuration has been saved'));

        $config = $this->config('paypal_plus.settings');
        $currency_code = $form_state->getValue('currency_code');
        $sandbox_mode = $form_state->getValue('sandbox_mode');
        $sclient_id = $form_state->getValue('sclient_id');
        $sclient_secret = $form_state->getValue('sclient_secret');
        $lclient_id = $form_state->getValue('lclient_id');
        $lclient_secret = $form_state->getValue('lclient_secret');
        $config->set('currency_code', $currency_code);
        $config->set('sandbox_mode', $sandbox_mode);
        $config->set('sclient_id', $sclient_id);
        $config->set('sclient_secret', $sclient_secret);
        $config->set('lclient_id', $lclient_id);
        $config->set('lclient_secret', $lclient_secret);
        $config->save();
    }

}
