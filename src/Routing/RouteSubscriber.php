<?php

namespace Drupal\paypal_plus\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase
{
    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(RouteCollection $collection)
    {
        // Hide specific routes from search engines and sitemap.
        $disabled_routes = [
          'paypal_plus.payment_success',
          'paypal_plus.payment_cancel',
        ];

        foreach ($disabled_routes as $disabled_route) {
            if ($route = $collection->get($disabled_route)) {
                $route->setOption('_no_index', true);
            }
        }
    }
}
