<?php

namespace Drupal\paypal_plus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Defines ZohoController class.
 */
class PaypalController extends ControllerBase
{
    /**
     * Callback URL handling for Sendinblue API API.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   Request.
     *
     * @return array
     *   Return markup for the page.
     */
    public function payment_success()
    {
        $token = \Drupal::request()->query->get('token');

        $success_redirect_url = '';

        // Invoke the custom hook for success redirect.
        \Drupal::moduleHandler()->invokeAll('paypal_plus_success_redirect', [&$success_redirect_url]);

        if($success_redirect_url == '') {
            $session = \Drupal::service('session');
            $success_redirect_url = $session->get('redirect_to');
            if($success_redirect_url == '') {
                $success_redirect_url = Url::fromRoute('<front>', [], ['absolute' => true])->toString();
            }
        }

        if($token != '') {

            $provider = \Drupal::service('paypal_plus');
            $provider->getAccessToken();
            $response = $provider->capturePaymentOrder($token);

            if (isset($response['status']) && $response['status'] == 'COMPLETED') {

                $message = '';

                // Invoke the custom hook for success message.
                \Drupal::moduleHandler()->invokeAll('paypal_plus_success_message', [&$message]);

                if($message == '') {
                    $message = '<strong>Success!</strong> Your payment has been successfully processed.';
                }

                \Drupal::messenger()->addMessage($this->t($message));

                $session = \Drupal::service('session');
                $session->set('item_ids', []);

                $response = new TrustedRedirectResponse($success_redirect_url);
                $response->send();

            } else {

                $failure_redirect_url = '';

                // Invoke the custom hook for failure redirect.
                \Drupal::moduleHandler()->invokeAll('paypal_plus_failure_redirect', [&$failure_redirect_url]);

                if($failure_redirect_url == '') {
                    $session = \Drupal::service('session');
                    $failure_redirect_url = $session->get('redirect_to');
                    if($failure_redirect_url == '') {
                        $failure_redirect_url = Url::fromRoute('<front>', [], ['absolute' => true])->toString();
                    }
                }

                $message = '';

                // Invoke the custom hook for failure message.
                \Drupal::moduleHandler()->invokeAll('paypal_plus_failure_message', [&$message]);

                if($message == '') {
                    $message = '<strong>Payment Failed!</strong> Unfortunately, we were unable to process your payment.';
                }

                \Drupal::messenger()->addMessage($this->t($message));

                $session = \Drupal::service('session');
                $session->set('item_ids', []);

                $response = new TrustedRedirectResponse($failure_redirect_url);
                // Redirect to the external URL
                $response->send();
            }
        } else {
            $response = new TrustedRedirectResponse($redirect_url);
            $response->send();
        }
    }

    /**
     * Callback URL handling for Sendinblue API API.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   Request.
     *
     * @return array
     *   Return markup for the page.
     */
    public function payment_cancel()
    {

        $ref = \Drupal::request()->query->get('ref');
        if($ref == 'paypal') {

            $cancel_redirect_url = '';

            // Invoke the custom hook for cancel redirect.
            \Drupal::moduleHandler()->invokeAll('paypal_plus_cancel_redirect', [&$cancel_redirect_url]);

            if($cancel_redirect_url == '') {
                $session = \Drupal::service('session');
                $cancel_redirect_url = $session->get('redirect_to');
                if($cancel_redirect_url == '') {
                    $cancel_redirect_url = Url::fromRoute('<front>', [], ['absolute' => true])->toString();
                }
            }

            $message = '';

            // Invoke the custom hook for cancel message.
            \Drupal::moduleHandler()->invokeAll('paypal_plus_cancel_message', [&$message]);

            if($message == '') {
                $message =  '<strong>Payment Canceled!</strong> Your transaction has been canceled.';
            }

            \Drupal::messenger()->addMessage($this->t($message));

            $session = \Drupal::service('session');
            $session->set('item_ids', []);

            $response = new TrustedRedirectResponse($cancel_redirect_url);
            $response->send();
        } else {
            throw new AccessDeniedHttpException();
        }
    }
}
