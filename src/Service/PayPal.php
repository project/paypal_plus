<?php

namespace Drupal\paypal_plus\Service;

use Exception;
use Drupal\paypal_plus\Traits\PayPalRequest as PayPalAPIRequest;
use Drupal\paypal_plus\Traits\PayPalVerifyIPN;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

class PayPal
{
    use PayPalAPIRequest;
    use PayPalVerifyIPN;
    use StringTranslationTrait;

    /**
     * PayPal constructor.
     *
     * @param array $config
     *
     * @throws Exception
     */
    public function __construct()
    {

        $current_url = Url::fromRoute('<current>', [], ['absolute' => true]);
        $full_url = $current_url->toString();

        $session = \Drupal::service('session');
        $session->set('redirect_to', $full_url);

        $paypal_plus_config = \Drupal::config('paypal_plus.settings');

        $currency_code = $paypal_plus_config->get('currency_code');
        $sandbox_mode = $paypal_plus_config->get('sandbox_mode');

        // Invoke the custom hook for currency code.
        \Drupal::moduleHandler()->invokeAll('paypal_plus_currency_code', [&$currency_code]);

        // Invoke the custom hook for sandbox mode.
        \Drupal::moduleHandler()->invokeAll('paypal_plus_sandbox_mode', [&$sandbox_mode]);

        if($sandbox_mode == '') {
            $sandbox_mode = true;
        }
        if($currency_code == '') {
            $currency_code = 'USD';
        }

        $sclient_id = $paypal_plus_config->get('sclient_id');
        $sclient_secret = $paypal_plus_config->get('sclient_secret');
        $lclient_id = $paypal_plus_config->get('lclient_id');
        $lclient_secret = $paypal_plus_config->get('lclient_secret');

        $mode = 'live';
        if($sandbox_mode) {
            $mode = 'sandbox';
        }

        $config = array(
         'mode' => $mode,
         'currency' => $currency_code,
         'sandbox' => array(
             'client_id' => $sclient_id,
             'PAYPAL_SANDBOX_API_PASSWORD' => '',
             'client_secret' => $sclient_secret,
             'PAYPAL_SANDBOX_API_CERTIFICATE' => '',
         ),
         'live' => array(
             'client_id' => $lclient_id,
             'PAYPAL_SANDBOX_API_PASSWORD' => '',
             'client_secret' => $lclient_secret,
             'PAYPAL_SANDBOX_API_CERTIFICATE' => '',
         ),
     );

        try {
            // Setting PayPal API Credentials
            $this->setConfig($config);

            $this->httpBodyParam = 'form_params';

            $this->options = [];
            $this->options['headers'] = [
             'Accept'            => 'application/json',
             'Accept-Language'   => $this->locale,
         ];
     } catch (Exception $e) {
        $message = $e->getMessage();
        log_paypal_plus_messages("warning", $message);
    }
}

    /**
     * Set ExpressCheckout API endpoints & options.
     *
     * @param array $credentials
     */
    protected function setOptions(array $credentials): void
    {

        // Setting API Endpoints
        $this->config['api_url'] = 'https://api-m.paypal.com';

        $this->config['gateway_url'] = 'https://www.paypal.com';
        $this->config['ipn_url'] = 'https://ipnpb.paypal.com/cgi-bin/webscr';

        if ($this->mode === 'sandbox') {
            $this->config['api_url'] = 'https://api-m.sandbox.paypal.com';

            $this->config['gateway_url'] = 'https://www.sandbox.paypal.com';
            $this->config['ipn_url'] = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';

            $this->config['client_id'] = $credentials['sandbox']['client_id'];
            $this->config['client_secret'] = $credentials['sandbox']['client_secret'];

        } elseif ($this->mode === 'live') {
            $this->config['client_id'] = $credentials['live']['client_id'];
            $this->config['client_secret'] = $credentials['live']['client_secret'];
        }

        // Adding params outside sandbox / live array
        $this->config['payment_action'] = $credentials['payment_action']??'';
        $this->config['notify_url'] = $credentials['notify_url']??'';
        $this->config['locale'] = $credentials['locale']??'';

    }

    public function paypalPay($payment_options)
    {
        if($this->access_token != null) {
            $amount = 1;
            if(isset($payment_options['amount']) && $payment_options['amount'] > 0) {
                $amount = $payment_options['amount'];
            }

            $currency_code = 'USD';
            if(isset($payment_options['currency_code']) && $payment_options['currency_code'] != '') {
                $currency_code = $payment_options['currency_code'];
            }

            $parameters = [];
            $success_route = 'paypal_plus.payment_success';
            $success_link = Url::fromRoute($success_route, $parameters)->setAbsolute();
            $return_url = $success_link->toString();

            $cancel_route = 'paypal_plus.payment_cancel';
            $parameters = ['ref'=>'paypal'];
            $cancel_link = Url::fromRoute($cancel_route, $parameters)->setAbsolute();
            $cancel_url = $cancel_link->toString();

            $item_ids = array();

        // Invoke the custom hook set order ids for future processing.
            \Drupal::moduleHandler()->invokeAll('paypal_plus_set_item_ids', [&$item_ids]);

            if(!empty($item_ids)) {
                $session = \Drupal::service('session');
                $session->set('item_ids', $item_ids);
            }

            $response = $this->createOrder(
                [
                    "intent" => "CAPTURE",
                    "application_context" => [
                     "return_url" => $return_url,
                     "cancel_url" => $cancel_url,
                 ],
                 "purchase_units" => [
                     0 => [
                         "amount" => [
                             "currency_code" => $currency_code,
                             "value" => $amount
                         ]
                     ]
                 ]
             ]
         );

            if (isset($response['id']) && $response['id'] != null) {
                foreach ($response['links'] as $links) {
                    if ($links['rel'] == 'approve') {
                        if(isset($links['href'])) {
                            $response = new TrustedRedirectResponse($links['href']);
                            $response->send();
                        }
                    }
                }

                $response = new TrustedRedirectResponse($cancel_url);
                $response->send();

            } else {
                $message = '';

            // Invoke the custom hook for failure message.
                \Drupal::moduleHandler()->invokeAll('paypal_plus_failure_message', [&$message]);

                if($message == '') {
                    $message = '<strong>Payment Failed!</strong> Unfortunately, we were unable to process your payment.';
                }

                \Drupal::messenger()->addMessage($this->t($message));

                $redirect_url = '';

            // Invoke the custom hook for failure redirect.
                \Drupal::moduleHandler()->invokeAll('paypal_plus_failure_redirect', [&$redirect_url]);

                if($redirect_url == '') {
                    $redirect_url = Url::fromRoute('<front>', [], ['absolute' => true])->toString();
                }

                $response = new TrustedRedirectResponse($redirect_url);
                $response->setStatusCode(RedirectResponse::HTTP_MOVED_PERMANENTLY);
                $response->send();
            }
        }
    }
}
