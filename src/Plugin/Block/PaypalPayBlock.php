<?php

namespace Drupal\paypal_plus\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "paypalpay_block",
 *   admin_label = @Translation("Paypal pay block"),
 *   category = @Translation("Paypal pay block")
 * )
 */
class PaypalPayBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $form = \Drupal::formBuilder()->getForm('Drupal\paypal_plus\Form\PaypalPayForm');
        return $form;
    }
}
