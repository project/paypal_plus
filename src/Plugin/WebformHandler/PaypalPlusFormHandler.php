<?php

namespace Drupal\paypal_plus\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\webformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "paypal_plus_handler",
 *   label = @Translation("Paypal Plus"),
 *   category = @Translation("Automated marketing"),
 *   description = @Translation("Send submission data to Paypal"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class PaypalPlusFormHandler extends WebformHandlerBase
{
    /**
     * The paypal plus api service.
     *
     * @var \Drupal\paypalplus\PaypalPlusApi
     */
    protected $paypalPlusApi;

    /**
     * {@inheritDoc}
     */
    public static function create(
        ContainerInterface $container,
        array $configuration,
        $plugin_id,
        $plugin_definition
    ) {
        $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
        $instance->paypalPlusApi = $container->get('paypal_plus');
        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration(): array
    {
        return [
          'currency_code' => 'USD',
          'amount' => '5',
      ];
  }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
    {

        $currency_codes = get_currency_codes();

        $form['currency_code'] = [
          '#type' => 'webform_select_other',
          '#title' => $this->t('Currency code'),
          '#required' => true,
          '#empty_option' => $this->t('- Select an option -'),
          '#default_value' => $this->configuration['currency_code'],
          '#options' => $currency_codes,
          '#ajax' => [
            'callback' => [$this, 'ajaxPaypalPlusHandler'],
            'wrapper' => 'webform-zoho_forms-handler-settings',
        ],
        '#description' => $this->t('Select the currency code you want to use.'),
    ];


    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $options = [];
    foreach ($fields as $field_name => $field) {
        if (!in_array($field['#type'], ['email', 'webform_email_confirm', 'submit'])) {
            $options[$field_name] = $field['#title'];
        }
    }

    $form['amount'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Amount'),
      '#required' => true,
      '#empty_option' => $this->t('- Select an option -'),
      '#default_value' => $this->configuration['amount'],
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'ajaxPaypalPlusHandler'],
        'wrapper' => 'webform-zoho_forms-handler-settings',
    ],
    '#description' => $this->t('Select the amount you want to use.'),
];

return $this->setSettingsParents($form);
}

    /**
     * Ajax callback to update Webform Zoho settings.
     */
    public static function ajaxPaypalPlusHandler(array $form, FormStateInterface $form_state)
    {
        return $form['settings']['development'];
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {

        parent::submitConfigurationForm($form, $form_state);

        // Save the configuration settings.
        $this->applyFormStateToConfiguration($form_state);

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {

        $configuration = $this->getConfiguration();
        $settings = $configuration['settings'];

        $data = $webform_submission->getData();

        // Retrieving the configured paypal plus fields from the data.
        $currency_code = $settings['currency_code'];
        $amount_key = $settings['amount'];

        $amount = $data[$amount_key]??null;

        if($amount == null) {
            $amount = $settings['amount'];
        }

        try {
            $provider = \Drupal::service('paypal_plus');

            $payment_options = array(
              'amount' => $amount,
              'currency_code' => $currency_code,
          );
            $provider->paypalPay($payment_options);
        } catch (\Exception $exception) {
            $this->getLogger('paypal_plus')->error($exception->getMessage());
        }

    }

}
